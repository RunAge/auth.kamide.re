const http = require('http');
const express = require('express');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const passport = require('passport');
const login = require('connect-ensure-login');
global.debug = require('debug');

const debug = global.debug('kamidere:index');

require('dotenv').config();

const app = express();

app.use(cookieParser(process.env.KAMIDERE_COOKIESECRET));
app.use(bodyParser.urlencoded({
  extended: false,
}));
app.use(bodyParser.json());
app.use(session({
  secret: process.env.KAMIDERE_COOKIESECRET,
  resave: false,
  saveUninitialized: false,
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static('public'));

require('./utils/passportInit');

app.get('/', (req, res) => {
  res.sendFile('public/index.html', { root: __dirname });
});

const server = http.createServer(app).listen(6643);

server.on('listening', () => {
  debug('Server listening on 6643');
});

const routes = {
  oauth2orize: require('./routes/oauth2orize'), // eslint-disable-line
  register: require('./routes/register'), // eslint-disable-line
};

app.get('/login', (req, res) => {
  res.sendFile('public/login.html', { root: __dirname });
});

app.post('/login', passport.authenticate('local', { successReturnToOrRedirect: '/me', failureRedirect: '/login' }));

app.get('/me', (req, res) => {
  res.sendFile('public/me.html', { root: __dirname });
});

app.get('/account', login.ensureLoggedIn(), (request, response) => response.json(
  {
    user: {
      username: request.user.username,
      displayName: request.user.displayName,
      email: request.user.email,
    },
  },
));
app.get('/dialog/authorize', routes.oauth2orize.auth);
app.post('/dialog/authorize/decision', routes.oauth2orize.decision);
app.post('/oauth/token', routes.oauth2orize.token);
app.get('/register', (req, res) => {
  res.sendFile('public/register.html', { root: __dirname });
});
app.post('/register', routes.register.createNewUser);
