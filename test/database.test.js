require('dotenv').config();
const path = require('path');
const db = require(path.join(process.cwd(), './utils/database'));
const assert = require('assert');

(async () => {
  try {
    const a = await db.Token.create({
      token: "aagfawgawwgawgawgawg",
      client: '5bf7d47c60caa33e7c7fccda',
      user: undefined
    })
    console.log(a)
  } catch (error) {
    console.log(error)
  }
})()


// This tests is written only for my DB :( and test user

describe('Database', () => {
  describe('User', () =>
    describe('findByUsername', () =>
      it('should return username from userObject',
        async () => assert.equal('test', (await db.User.findByUsername('test').exec()).username)
      )
    )
  );

  describe('User', () =>
    describe('findById', () =>
      it('should return email from userObject',
        async () => assert.equal('testE', (await db.User.findById('5bf7ca3760caa33e7c7fccd5').exec()).email)
      )
    )
  );

  describe('User', () =>
    describe('findById', () =>
      it('should return first connection name from userObject',
        async () => assert.equal('GoddessApp', (await db.User.findById('5bf7ca3760caa33e7c7fccd5').populate('connections').exec()).connections[0].name)
      )
    )
  );

  describe('User', () =>
    describe('findById', () =>
      it('should return first token from userObject',
        async () => assert.equal('testToken', (await db.User.findById('5bf7ca3760caa33e7c7fccd5').populate('tokens').exec()).tokens[0].token)
      )
    )
  );

  describe('Token', () =>
    describe('findByToken', () =>
      it('should return username from tokenObject',
        async () => assert.equal('test', (await db.Token.findByToken('testToken').populate('user').exec()).user.username)
      )
    )
  );

  describe('Token', () =>
    describe('findByToken', () =>
      it('should NOT return username from tokenObject',
        async () => assert.equal(undefined, (await db.Token.findByToken('testToken').exec()).user.username)
      )
    )
  );

  describe('Token', () =>
    describe('findByToken', () =>
      it('should return client _id from tokenObject',
        async () => assert.equal('5bf7d47c60caa33e7c7fccda', String((await db.Token.findByToken('testToken').populate('client').exec()).client._id))
      )
    )
  );

  describe('Token', () =>
    describe('findByToken', () =>
      it('should return token from tokenObject',
        async () => assert.equal('testToken', (await db.Token.findByToken('testToken').exec()).token)
      )
    )
  );
  describe('Client', () =>
    describe('findById', () =>
      it('should return name from clientObject',
        async () => assert.equal('GoddessApp', (await db.Client.findById('5bf7d47c60caa33e7c7fccda').exec()).name)
      )
    )
  );

  describe('Code', () =>
    describe('findByCode', () =>
      it('should return code from codeObject',
        async () => assert.equal('e2ee4594afe8704da308dc04409181b3', String((await db.Code.findByCode('e2ee4594afe8704da308dc04409181b3').exec()).code))
      )
    )
  );

  describe('Code', () =>
    describe('findByCode', () =>
      it('should return User username from codeObject',
        async () => assert.equal('test', (await db.Code.findByCode('e2ee4594afe8704da308dc04409181b3').populate('user').exec()).user.username)
      )
    )
  );

  describe('Code', () =>
    describe('findByCode', () =>
      it('should return User id from codeObject',
        async () => assert.equal('5bf7ca3760caa33e7c7fccd5', String((await db.Code.findByCode('e2ee4594afe8704da308dc04409181b3').exec()).user))
      )
    )
  );
});
