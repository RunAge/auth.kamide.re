const oauth2orize = require('oauth2orize');
const passport = require('passport');
const login = require('connect-ensure-login');
const jwt = require('jsonwebtoken');
const fs = require('fs');
const sodium = require('sodium').api;
const db = require('../utils/database');

const signKey = fs.readFileSync('./certs/jwt.key');
const server = oauth2orize.createServer();

server.serializeClient((client, done) => done(null, client.id));

server.deserializeClient(
  async (id, done) => {
    try {
      const client = await db.Client.findById(id).exec();
      if (client && client.id) return done(null, client);
      done(null, false);
    } catch (error) {
      done(error);
    }
  },
);

function genToken(client, user = { id: 'NONE' }) {
  return String(jwt.sign({
    client: client.id,
    user: user.id,
    unique: user.unique,
  },
  {
    key: signKey,
    passphrase: process.env.KAMIDERE_JWTSECRET,
  },
  {
    algorithm: 'RS256',
    expiresIn: '1y',
    keyid: 'UniqueID',
  }));
}

// Grant authorization codes.
server.grant(oauth2orize.grant.code(
  async (client, redirectUri, user, done) => {
    if (redirectUri !== client.redirectURI) return done(null, false);
    try {
      const genCode = Buffer.allocUnsafe(16);
      sodium.randombytes_buf(genCode, 16);
      const code = await db.Code.create({
        user: user.id,
        client: client.id,
        code: genCode.toString('hex'),
      });
      done(null, code.code);
    } catch (error) {
      done(error);
    }
  },
));

// Grant implicit authorization.
server.grant(
  oauth2orize.grant.token(
    async (client, user, done) => {
      try {
        const token = await db.Token.create({
          token: genToken(client, user),
          user: user.id,
          client: client.id,
        });

        done(null, token.token);
      } catch (error) {
        done(error);
      }
    },
  ),
);

// Exchange authorization codes for access tokens.
server.exchange(oauth2orize.exchange.code(
  async (client, code, redirectUri, done) => {
    try {
      const dbCode = await db.Code.findByCode(code).exec();
      if (client._id !== dbCode.client) return done(null, false);
      if (redirectUri !== dbCode.redirectURI) return done(null, false);
      const token = await db.Token.create({
        token: genToken(client, user),
        user: user._id,
        client: client._id,
      });
      done(null, token.token);
    } catch (error) {
      done(error);
    }
  },
));

// Exchange user id and password for access tokens.
server.exchange(oauth2orize.exchange.password(
  async (client, username, pwd, done) => {
    try {
      const lClient = await db.Client.findById(client.id).exec();
      if (!lClient) return done(null, false);
      if (client.secret !== lClient.secret) return done(null, false);

      const user = await db.User.findByUsername(username).exec();
      if (!user) return done(null, false);

      const seed = Buffer.from(user.seed, 'hex');
      const pwdhash = sodium.crypto_auth_hmacsha256(Buffer.from(pwd), seed).toString('hex');
      if (!user || pwdhash !== user.pwd) return done(null, false);

      const token = await db.Token.create({
        token: genToken(client, user),
        user: user._id,
        client: client._id,
      });
      done(null, token.token);
    } catch (error) {
      done(error);
    }
  },
));

// Exchange the client id and password/secret for an access token.
server.exchange(oauth2orize.exchange.clientCredentials(
  async (client, done) => {
    try {
      const lClient = await db.Client.findById(client.id).exec();
      if (!lClient) return done(null, false);
      if (client.secret !== lClient.secret) return done(null, false);
      const token = await db.Token.create({
        token: genToken(client),
        client: client._id,
        user: undefined,
      });
      done(null, token.token);
    } catch (error) {
      done(error);
    }
  },
));

const auth = [
  login.ensureLoggedIn(),
  server.authorization(
    async (id, redirectUri, done) => {
      try {
        const client = await db.Client.findById(id).exec();
        if (redirectUri !== client.redirectURI) return done(null, false);
        return done(null, client, redirectUri);
      } catch (error) {
        done(error);
      }
    },
    // async (client, user, done) => {
    //   const token = await db.Token.findOne({ user: user._id, client: client._id }).exec();
    //   if (token) return done(null, true)
    //   return done(null, false)
    // }
  ),
  (req, res) => {
    res.json({ transactionId: req.oauth2.transactionID, user: req.user, client: req.oauth2.client });
  },
];
const decision = [
  login.ensureLoggedIn(),
  server.decision(),
];

const token = [
  passport.authenticate(['basic', 'oauth2-client-password'], { session: false }),
  server.token(),
  server.errorHandler(),
];

module.exports = {
  auth,
  decision,
  token,
};
