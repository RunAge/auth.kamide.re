# kamidere-oauth2server

Authorization service for future projects. 
ref. [oauth2orize-example](https://github.com/gerges-beshay/oauth2orize-examples)

# TODO
* Learn about good hashing practice and implement it 
  * sodium ~~or libsodium-wrappers~~ (ref. https://crackstation.net/hashing-security.htm)
  * clinet-side prehashing (ref. https://download.libsodium.org/doc/password_hashing)
* /register endpoint with both GET and POST
* /client, /me, endpoints
* Good looking front-end
  * Vue.js?


# IDEA
* Minimal information about user [username, email, password].
* Client-side tokens (stored on client-side but generated on server side).
* End-to-End encryption [user ←→ server, client ←→ server] (optional).
* Two-Factor Authentication (optional).
* Support to 3rd Party Authorization [Google, Anilist, etc] (optional).

# FEATURES
* Secure users even when security breach:
  * E-mail hashing.
  * Password hashing.
  * Hashing specified in .env file.
  * Tokens stored only on client side.
  * Unique hash for user/client to secure tokens.
    * Changing this hash will automatically disable every token connected with it. 
    Will logout you from every website or will logout every user from your service.

# .env
```
KAMIDERE_MONGOURI= mongoDB URI.
KAMIDERE_JWTSECRET= JWT secret.
KAMIDERE_COOKIESECRET= Cookeies secret.
KAMIDERE_SEEDSINDEX= Seeds index array [PASSWORD SEED, CLIENT SIDE SEED, EMIAL SEED]. ex. [0,1,2]
```