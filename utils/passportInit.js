const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const { BasicStrategy } = require('passport-http');
const ClientPasswordStrategy = require('passport-oauth2-client-password').Strategy;
const BearerStrategy = require('passport-http-bearer').Strategy;
const sodium = require('sodium').api;

const seedsIndex = JSON.parse(process.env.KAMIDERE_SEEDSINDEX);
const jwt = require('jsonwebtoken');
const fs = require('fs');
const db = require('./database');

const jwtPublicKey = fs.readFileSync('./certs/jwt.key.pub');

passport.use(new LocalStrategy(
  async (username, pwd, done) => {
    username = username.toLowerCase();
    try {
      const user = await db.User.findByUsername(username).exec();

      if (!user) { done(null, false); }

      const seed = Buffer.from(user.seeds[seedsIndex[0]], 'hex');

      pwd = Buffer.from(pwd);
      const pwdhash = sodium.crypto_auth_hmacsha256(pwd, seed).toString('hex');

      if (!user || pwdhash !== user.pwd) { return done(null, false); }

      done(null, user);
    } catch (error) {
      done(error);
    }
  },
));

passport.serializeUser((user, done) => {
  done(null, user._id);
});

passport.deserializeUser(
  async (id, done) => {
    try {
      const user = await db.User.findById(id).exec();

      if (user && user._id) { return done(null, user); }
    } catch (error) {
      done(error);
    }
  },
);

async function verifyClient(id, secret, done) {
  try {
    const client = await db.Client.findById(id).exec();

    if (!client || secret !== client.secret) { return done(null, false); }

    done(null, client);
  } catch (error) {
    done(error);
  }
}

passport.use(new BasicStrategy(verifyClient));

passport.use(new ClientPasswordStrategy(verifyClient));

// REWORK
passport.use(new BearerStrategy(
  async (token, done) => {
    const JWToken = jwt.verify(token, jwtPublicKey);
    if (!JWToken) { return done(null, false); }

    try {
      token.user._id ? userToken(token) : clientToken(token);
    } catch (error) {
      done(error);
    }

    function userToken(token) {
      if (!token.user) { return done(null, false); }

      done(null, token.user);
    }

    function clientToken(token) {
      if (!token.client) { return done(null, false); }

      done(null, token.client);
    }
  },
));
