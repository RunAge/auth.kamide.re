const mongoose = require('mongoose');

const debug = global.debug('kamidere:database');

/**
 * Connecting to mongoDB server.
 */
function mongooseConnect() {
  mongoose.connect(process.env.KAMIDERE_MONGOURI, { useNewUrlParser: true }).catch((error) => {
    debug(error);
    process.exit(1);
  });
}

mongooseConnect();
const conn = mongoose.connection;
const ObjectId = mongoose.Schema.Types.ObjectId; // eslint-disable-line

conn.on('disconnect', () => {
  mongooseConnect();
});


/**
 * @param {Object} user
 * @param {String} user.username Username
 * @param {String} user.displayName Display name
 * @param {String} user.pwd Password hash
 * @param {String} user.seed Password seed
 * @param {String} user.email Email hash
 * @param {Schema.Types.ObjectId} user.connections Connections array
 */
const userScheme = new mongoose.Schema({
  username: String,
  displayName: String,
  pwd: String,
  email: String,
  seeds: Array,
  unique: String,
  connections: [{ type: ObjectId, ref: 'clients' }],
});

/**
 * @param {Object} client
 * @param {String} client.secret Secret
 * @param {String} client.redirectURI redirectURI
 * @param {String} client.name Client name
 * @param {String} client.desc Client descryption
 */
const clientScheme = new mongoose.Schema({
  secret: String,
  redirectURI: String,
  name: String,
  desc: String,
  unique: String,
});


/**
 * @param {Object} code
 * @param {Schema.Types.ObjectId} code.user User object
 * @param {Schema.Types.ObjectId} code.client Client object
 * @param {String} code.redirectURI redirectURI
 */
const codeScheme = new mongoose.Schema({
  code: String,
  user: { type: ObjectId, ref: 'users' },
  client: { type: ObjectId, ref: 'clients' },
  redirectURI: String,
});

const User = mongoose.model('users', userScheme);
const Client = mongoose.model('clients', clientScheme);
const Code = mongoose.model('codes', codeScheme);

/**
 * Finds a single document by its username field.
 * @param {String} username
 * @return {Query} Quary
 */
User.findByUsername = function findByUsername(username) {
  return this.findOne({ username });
};

/**
 * Finds a single document by its code field.
 * @param {String} code
 * @return {Query} Quary
 */
Code.findByCode = function findByCode(code) {
  return this.findOne({ code });
};

module.exports = {
  User,
  Client,
  Code,
};
